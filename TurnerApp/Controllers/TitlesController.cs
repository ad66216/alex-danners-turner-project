﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TurnerApp.Controllers
{
    public class TitlesController : Controller
    {
        private TitlesDBEntities db = new TitlesDBEntities();

        //Below we use .Take to take just the first 10 Titles that match the criteria
        //We use "Select" to do a project and turn every Movie into an object that has a label property 
        //and that label property will be equal to the Name of the Movie.
        //We create something with a label property because the jQuery UI documentation
        //says the JSON returned should have an object with a label property or a value property or both
        public ActionResult AutoComplete(string term)
        {
            var model = db.Titles
                .Where(r => r.TitleNameSortable.StartsWith(term))
                .Take(10)
                .Select(r => new
                {
                    label = r.TitleName
                });
            //We now need to put it in JSON format.  We do so by invoking the JSON method
            //This will serialize the model into JSON format.
            //We allow this to happen during a GET request
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TitleIndex(string searchTerm = null)
        {
            //return View(db.Titles.ToList());

            var model = db.Titles
                .OrderBy(r => r.TitleNameSortable)
                .Where(r => searchTerm == null || r.TitleNameSortable.StartsWith(searchTerm));

            //return View(model);
            return View(model);
        }

        //
        // GET: /Titles/Details/5

        public ActionResult Details(int id = 0)
        {
            Title title = db.Titles.Find(id);
            if (title == null)
            {
                return HttpNotFound();
            }
            return View(title);
        }

        //
        // GET: /Titles/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Titles/Create

        [HttpPost]
        public ActionResult Create(Title title)
        {
            if (ModelState.IsValid)
            {
                db.Titles.Add(title);
                db.SaveChanges();
                return RedirectToAction("TitleIndex");
            }

            return View(title);
        }

        //
        // GET: /Titles/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Title title = db.Titles.Find(id);
            if (title == null)
            {
                return HttpNotFound();
            }
            return View(title);
        }

        //
        // POST: /Titles/Edit/5

        [HttpPost]
        public ActionResult Edit(Title title)
        {
            if (ModelState.IsValid)
            {
                db.Entry(title).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("TitleIndex");
            }
            return View(title);
        }

        //
        // GET: /Titles/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Title title = db.Titles.Find(id);
            if (title == null)
            {
                return HttpNotFound();
            }
            return View(title);
        }

        //
        // POST: /Titles/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Title title = db.Titles.Find(id);
            db.Titles.Remove(title);
            db.SaveChanges();
            return RedirectToAction("TitleIndex");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}