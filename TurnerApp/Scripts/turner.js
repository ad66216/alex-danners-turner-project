﻿$(function () {

    var createAutocomplete = function () {

        var $input = $(this);

        var options = {
            source: $input.attr("data-turner-autocomplete")
        };

        $input.autocomplete(options);
    };

    $("form [data-turner-autocomplete]").each(createAutocomplete);
});