﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TurnerApp.Models
{
    public class Title
    {
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public string TitleNameSortable { get; set; }
        public int TitleTypeId { get; set; }
        public int ReleaseYear { get; set; }
        public string ProcessedDateTimeUTC { get; set; }

    }
}